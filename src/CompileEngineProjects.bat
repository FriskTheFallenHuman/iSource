@echo OFF
cls
MSBuild.exe /m /p:Platform="x86" /p:Configuration=Release external/vpc/vpc.sln
if ERRORLEVEL 1 (
  echo MsBuild not found in PATH. Please, start from Developer Command Prompt or add MSVC MsBuild directory to the PATH.
  exit /B 1
)
@pause