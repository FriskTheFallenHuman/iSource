@echo OFF
set VALVE_NO_AUTO_P4=1
cls
MSBuild.exe /m /p:Platform="x86" /p:Configuration=Release external/vpc/vpc.sln
if ERRORLEVEL 1 (
  echo MsBuild not found in PATH. Please, start from Developer Command Prompt or add MSVC MsBuild directory to the PATH.
  exit /B 1
)

devtools\bin\vpc.exe /2015 /define:WORKSHOP_IMPORT_DISABLE /define:SIXENSE_DISABLE /define:NO_X360_XDK /define:RAD_TELEMETRY_DISABLED /define:DISABLE_ETW /define:LTCG /no_ceg /retail /define:CERT /tf +game /nop4add /mksln games.sln
@timeout /t 30