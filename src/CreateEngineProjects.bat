@echo OFF
set VALVE_NO_AUTO_P4=1
cls
MSBuild.exe /m /p:Platform="x86" /p:Configuration=Release external/vpc/vpc.sln
if ERRORLEVEL 1 (
  echo MsBuild not found in PATH. Please, start from Developer Command Prompt or add MSVC MsBuild directory to the PATH.
  exit /B 1
)

devtools\bin\vpc.exe /2015 /define:WORKSHOP_IMPORT_DISABLE /define:SIXENSE_DISABLE /define:NO_X360_XDK /define:RAD_TELEMETRY_DISABLED /define:DISABLE_ETW /define:LTCG /no_ceg /retail /define:CERT -classcheck -cubelight -dbmon -dumpmatsyshelp -ep2_deathmap -getbugs -glview -localization_check -makegamedata -makescenesimage -matsys_regressiontest -mdlcheck -newdat -p4lib -paginate -panel_zoo -psdinfo -sampletool -sceneviewer -scratchpad3dviewer -simdtest -smtpmail -stdshader_dx6 -stdshader_dx7 -symbolstoreupdate -tagbuild -testprocess -texturesynth -tgadiff -tgamse -tier1test -tier2test -tier3test -unusedcontent -vcd_sound_check -vlocalize -vmf_tweak -vmtcheck -vp4 -vstOverlap_maya2009 -vtfdiff -vtfscrew -socketlib -bugreporter -bugreporter_filequeue -bugreporter_public -bugreporter_text +sourcelicensee /nop4add /mksln Engine.sln
@timeout /t 30